package at.mavila.learn.kafka.kafkaexercises;


import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class DuplicatePersonFilterTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuplicatePersonFilterTest.class);



    @Test
    public void testList(){

        Person alex = new Person.Builder().id(1L).firstName("alex").secondName("salgado").build();
        Person lolita = new Person.Builder().id(2L).firstName("lolita").secondName("llanero").build();
        Person elpidio = new Person.Builder().id(3L).firstName("elpidio").secondName("ramirez").build();
        Person romualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("gomez").build();
        Person otroRomualdo = new Person.Builder().id(4L).firstName("romualdo").secondName("perez").build();


        List<Person> personList = new ArrayList<>();

        personList.add(alex);
        personList.add(lolita);
        personList.add(elpidio);
        personList.add(romualdo);
        personList.add(otroRomualdo);

        final List<Person> duplicates = DuplicatePersonFilter.getDuplicates(personList);

        LOGGER.info("Duplicates: {}",duplicates);

    }

}