package at.mavila.learn.kafka.kafkaexercises;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Person {

    private final Long id;
    private final String firstName;
    private final String secondName;


    private Person(final Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.secondName = builder.secondName;
    }


    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public static class Builder {

        private Long id;
        private String firstName;
        private String secondName;

        public Builder id(final Long builder) {
            this.id = builder;
            return this;
        }

        public Builder firstName(final String first) {
            this.firstName = first;
            return this;
        }

        public Builder secondName(final String second) {
            this.secondName = second;
            return this;
        }

        public Person build() {
            return new Person(this);
        }


    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("firstName", firstName)
                .append("secondName", secondName)
                .toString();
    }
}
