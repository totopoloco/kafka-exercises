package at.mavila.learn.kafka.kafkaexercises;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public final class DuplicatePersonFilter {


    private DuplicatePersonFilter() {
        //No instances of this class
    }

    public static List<Person> getDuplicates(final List<Person> personList) {

       return personList
               .stream()
               .filter(duplicateByKey(Person::getId))
               .filter(duplicateByKey(Person::getFirstName))
               .collect(Collectors.toList());

    }

    private static <T> Predicate<T> duplicateByKey(final Function<? super T, Object> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> isNull(seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE));

    }

}
