package at.mavila.learn.kafka.kafkaexercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaExercisesApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaExercisesApplication.class, args);
	}
}
